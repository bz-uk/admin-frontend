const path = require("path");
const aliases = require("./webpack-aliases");
module.exports = {
  configureWebpack: {
    resolve: {
      alias: aliases.resolve.alias
    },
  },
  transpileDependencies: ["vuetify", "vue-clamp", "resize-detector"],
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule.use("vue-svg-loader").loader("vue-svg-loader");

    // config.resolve.alias
    //     .set('@components', path.resolve(__dirname, "src/components"));
  },
  pages: {
    index: {
      entry: "src/main.js",
      template: "public/index.html",
      filename: "index.html"
    },
    login: {
      entry: "src/login.js",
      template: "public/index.html",
      filename: "login.html"
    }
  }
};
