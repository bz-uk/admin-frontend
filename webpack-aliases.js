/*
 * Copyright (c) 2019. Botanická zahrada Přírodovědecké fakulty Univerzity Karlovy, Juraj Šimon
 */
const path = require("path");

module.exports = {
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@components": path.resolve(__dirname, "src/components"),
      "@services": path.resolve(__dirname, "src/services")
    }
  }
};
