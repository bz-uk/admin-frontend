export function getBooleanWithDefault(name, defaultValue) {
  let val = JSON.parse(localStorage.getItem(name));
  if (typeof val !== "boolean") {
    return defaultValue;
  }
  return val;
}

export function getIntWithDefault(name, defaultValue) {
  let val = JSON.parse(localStorage.getItem(name));
  if (!Number.isInteger(val)) {
    return defaultValue;
  }
  return val;
}

export function getStringWithDefault(name, defaultValue) {
  let val = JSON.parse(localStorage.getItem(name));
  if (typeof val !== "string") {
    return defaultValue;
  }
  return val;
}

export function getArrayWithDefault(name, defaultValue) {
  let val = JSON.parse(localStorage.getItem(name));
  if (!Array.isArray(val)) {
    return defaultValue;
  }
  return val;
}

export function getObjectWithDefault(name, defaultValue) {
  let val = JSON.parse(localStorage.getItem(name));
  if (typeof val !== "object" || val === null) {
    return defaultValue;
  }
  return val;
}
