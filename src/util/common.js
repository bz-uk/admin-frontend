import Vue from "../init";

export function cloneObject(obj) {
  return JSON.parse(JSON.stringify(obj));
}

export async function copyTextToClipboard(text) {
  try {
    await navigator.clipboard.write(text);
    Vue.notify({
      title: "Notifikace",
      text: "Zkopírováno do schránky",
      group: "app",
      type: "info"
    });
  } catch (e) {
    Vue.notify({
      title: "Chyba",
      text: "Kopírování do schránky zlyhalo",
      group: "app",
      type: "info"
    });
    console.error("Fallback: Oops, unable to copy", e);
  }

  // const textArea = document.createElement("textarea");
  // textArea.value = text;
  // textArea.style.position = "fixed";
  // document.body.appendChild(textArea);
  // textArea.focus();
  // textArea.select();
  //
  // try {
  //   let successful = document.execCommand("copy");
  //   if (successful) {
  //     Vue.notify({
  //       title: "Notifikace",
  //       text: "Zkopírováno do schránky",
  //       group: "app",
  //       type: "info"
  //     });
  //   }
  // } catch (err) {
  //   console.error("Fallback: Oops, unable to copy", err);
  // }
  //
  // document.body.removeChild(textArea);
}
