/*
 * Copyright (c) 2019. Botanická zahrada Přírodovědecké fakulty Univerzity Karlovy, Juraj Šimon
 */

import "./modernizr-custom";

function detectAndUpdateBody() {
  return new Promise(resolve => {
    // eslint-disable-next-line no-undef
    Modernizr.on("webp", function(result) {
      document
        .getElementsByTagName("body")[0]
        .classList.add(result ? "webp" : "no-webp");
      return resolve(result);
    });
  });
}

export { detectAndUpdateBody };
