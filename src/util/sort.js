export function sortObjectsArrayByKey(items, keyName = "id", desc = false) {
  let sortedArray = items.slice();
  sortedArray.sort((a, b) => {
    if (desc) {
      if (a[keyName] > b[keyName]) {
        return -1;
      } else if (a[keyName] < b[keyName]) {
        return 1;
      } else {
        return 0;
      }
    } else {
      if (a[keyName] < b[keyName]) {
        return -1;
      } else if (a[keyName] > b[keyName]) {
        return 1;
      } else {
        return 0;
      }
    }
  });
  return sortedArray;
}
