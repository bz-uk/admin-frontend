import urlJoin from "proper-url-join";

export function getImagePreviewUrl(imageId, width = 640, webp = false) {
  return urlJoin(
    process.env.VUE_APP_DOMAIN,
    "images-preview",
    imageId,
    `max-width/${width}`,
    webp ? "webp" : ""
  );
}
