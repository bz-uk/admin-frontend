import slugify from "slugify";
export function createSlug(string) {
  return slugify(string, {
    lower: true,
    replacement: "-",
    remove: /[*+~.()'"!:@;]/g
  });
}
