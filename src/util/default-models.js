import { getDefaultLanguage } from "./language";

export function createEmptyArticle() {
  let article = {
    ArticleTranslations: [
      {
        language_id: getDefaultLanguage().id,
        published: false
      }
    ]
  };
  return article;
}
