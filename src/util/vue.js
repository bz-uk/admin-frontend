import Vue from "../init";
import vuetify from "../plugins/vuetify";
import store from "../store";
import VueI18n from "vue-i18n";
import { i18nMessages as messages } from "../i18n";

export function createAndMountVue(component) {
  const vueInstance = new Vue({
    vuetify,
    store,
    i18n: new VueI18n({ locale: "cs", messages }),
    render: h => h(component)
  });
  vueInstance.$mount("#app");
}
