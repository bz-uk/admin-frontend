import store from "./../store";

const DEFAULT_LANGUAGE = "cs";

export function getLanguageById(languageId) {
  let languagesList = store.getters["languages/languages"];
  let langCodes = Object.keys(languagesList);
  for (let code of langCodes) {
    if (languagesList[code].id === languageId) {
      return languagesList[code];
    }
  }
  throw new Error(`Language with id ${languageId} does not exist!`);
}

export function getArticlesPrefixByLanguage(languageId) {
  let lang = getLanguageById(languageId);
  if (lang.two_letter_code === "cs") {
    return "clanky";
  }
  return "articles";
}

export function getDefaultLanguage() {
  let languagesList = store.getters["languages/languages"];
  return languagesList[DEFAULT_LANGUAGE];
}

export function getAllLanguagesIds() {
  let languagesList = store.getters["languages/languages"];
  let ids = [];
  let codes = Object.keys(languagesList);
  for (let code of codes) {
    ids.push(languagesList[code].id);
  }
  return ids;
}

//todo: load from server
export const DEFAULT_LANGUAGE_CODE = DEFAULT_LANGUAGE;
