export function getGalleryWidgetIntegrationCode(apiKey, galleryId) {
  return `
    <!--
Pro editaci teto galerie se prosim prihlaste do adminu
https://admin.bz-uk.roamingowl.com/plants
( v pripade problemu piste jurajsim@gmail.com)
-->
<!--use this as template for integration into drupal-->
<div id="widget" class="gallery-widget" apiKey="${apiKey}" galleryId="${galleryId}" registryUrl="http://registr.bz-uk.roamingowl.com" apiEndpoint="https://registr.bz-uk.roamingowl.com/widgets/dp-gallery/"/>
<script src="//registr.bz-uk.roamingowl.com/widgets/drupal_gallery_react.js"></script>
<script>
    var link = document.createElement( "link" );
    link.href = "//registr.bz-uk.roamingowl.com/widgets/drupal_gallery_react.css"
    link.type = "text/css";
    link.rel = "stylesheet";
    link.media = "screen,print";
    document.getElementsByTagName( "head" )[0].appendChild( link );
</script>`;
}
