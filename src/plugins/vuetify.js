import Vue from "../init";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({});
