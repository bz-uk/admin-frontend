/*
 * Copyright (c) 2019. Botanická zahrada Přírodovědecké fakulty Univerzity Karlovy, Juraj Šimon
 */

import Vue from "./init";
import Router from "vue-router";

Vue.use(Router);

const ROUTES = {
  HOME: "Home",
  PLANTS_LIST: "Plants list",
  TAGS_LIST: "Tags list",
  CATEGORIES_LIST: "Categories list",
  OPENING_HOURS: "Opening hours",
  FAMILIES_LIST: "Families list",
  AUTHORS_LIST: "Authors list",
  ARTICLES_LIST: "Articles list",
  ARTICLE_EDIT: "Article edit",
  GALLERIES_LIST: "Galleries list",
  GALLERY_EDIT: "Gallery edit",
  ABOUT: "About"
};

function create() {
  const router = new Router({
    mode: "history",
    routes: [
      {
        path: "/plants",
        name: ROUTES.PLANTS_LIST,
        component: () => import("./components/pages/plants-list.vue")
      },
      {
        path: "/dashboard",
        name: ROUTES.HOME,
        component: () => import("./components/pages/home.vue")
      },
      {
        path: "/manage/about",
        name: ROUTES.ABOUT,
        component: () => import("./components/pages/about")
      },
      {
        path: "/manage/tags",
        name: ROUTES.TAGS_LIST,
        component: () => import("./components/pages/tags-list.vue")
      },
      {
        path: "/manage/articles",
        name: ROUTES.ARTICLES_LIST,
        component: () => import("./components/pages/articles-list.vue")
      },
      {
        path: "/manage/articles/:id",
        name: ROUTES.ARTICLE_EDIT,
        component: () => import("./components/pages/article")
      },
      {
        path: "/manage/categories",
        name: ROUTES.CATEGORIES_LIST,
        component: () => import("./components/pages/categories-list.vue")
      },
      {
        path: "/manage/opening-hours",
        name: ROUTES.OPENING_HOURS,
        component: () => import("./components/pages/opening-hours")
      },
      {
        path: "/manage/authors",
        name: ROUTES.AUTHORS_LIST,
        component: () => import("./components/pages/authors-list.vue")
      },
      {
        path: "/manage/galleries",
        name: ROUTES.GALLERIES_LIST,
        component: () => import("./components/pages/galleries-list")
      },
      {
        path: "/manage/gallery/:id",
        name: ROUTES.GALLERY_EDIT,
        component: () => import("./components/pages/gallery")
      },
      {
        path: "/manage/families",
        name: ROUTES.FAMILIES_LIST,
        component: () => import("./components/pages/families-list")
      },
      { path: "/", redirect: "/dashboard" }
    ]
  });
  return router;
}

export { create, ROUTES };
