import Vue from "./init";
import App from "./components/authorized-app";
import vuetify from "./plugins/vuetify";
import { create as createRouterInstance } from "./routes";
import store from "./store";
import { detectAndUpdateBody as detectWebp } from "./util/webp";
import VueI18n from "vue-i18n";
import { extend as extendValidator } from "vee-validate";
import { max, required } from "vee-validate/dist/rules";

import { i18nMessages as messages } from "./i18n";

import "./styles/index.scss";

Vue.config.productionTip = false;

//validators
extendValidator("required", { ...required, message: "Hodnota je povinná" });
extendValidator("length", {
  ...max,
  message: "Max. délka hodnoty je {length} znaků"
});

class CustomI18n extends VueI18n {
  getChoiceIndex(choice, choicesLength) {
    // this === VueI18n instance, so the locale property also exists here
    if (this.locale !== "cs") {
      return super.getChoiceIndex(choice, choicesLength);
    }

    if (choicesLength === 4) {
      //case: zadnej tag | jeden tag | 2-4 tagy | 5+ tagu
      if (choice < 2) {
        // zadnej tag, jeden tag
        return choice;
      }
      if (choice >= 2 && choice < 5) {
        //dva tagy, tri tagy, ctyri tagy
        return 2;
      }
      if (choice > 5) {
        return 3;
      }
    } else {
      //case: zadnej/jeden tag | 2-4 tagy | 5+ tagu
      if (choice < 2) {
        // zadnej tag, jeden tag
        return 0;
      }
      if (choice >= 2 && choice < 5) {
        //dva tagy, tri tagy, ctyri tagy
        return 1;
      }
      if (choice > 5) {
        return 2;
      }
    }
  }
}
Vue.use(CustomI18n);

function mount(component = App) {
  const vueInstance = new Vue({
    vuetify,
    store,
    i18n: new CustomI18n({
      locale: "cs",
      messages,
      silentTranslationWarn: true
    }),
    router: createRouterInstance(),
    render: h => h(component)
  });
  vueInstance.$mount("#app");
}

function loadSettings(store) {
  store.dispatch("app/loadSettings");
  setTimeout(() => {
    loadSettings(store);
  }, 30000);
}

async function start() {
  let webpSupported = await detectWebp();
  loadSettings(store);
  store.dispatch("app/setWebpSupport", webpSupported);
  mount();
}

start();
