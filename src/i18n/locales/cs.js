const i18nMessagesCs = {
  tag: "tag | tagy | tagů",
  author: "autor | autoři | autorů",
  article: "článek | články | článků",
  plant: "rostlina | rostliny | rostlin",
  family: "čeleď | čeledi | čeledí",
  category: "kategorie | kategorie | kategorí",
  search: "hledat",
  foundArticles:
    "Nenalezeny žádné články | Nenalezen {n} článek | Nenalezeny {n} články | Nenalezeno {n} článků",
  foundGalleries:
    "Nenalezeny žádné galerie | Nenalezena {n} galerie | Nenalezeny {n} galerie | Nenalezeno {n} galerií",
  "Contains n images":
    "Neobsahuje žádné obrázky | Obsahuje {n} obrázek | Obsahuje {n} obrázky | Obsahuje {n} obrázků"
};

export { i18nMessagesCs };
