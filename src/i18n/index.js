import { i18nMessagesCs } from "./locales/cs";

const i18nMessages = {
  cs: i18nMessagesCs
};

export { i18nMessages };
