import Vue from "vue";
import Vuex from "vuex";
import VueResource from "vue-resource";
import Notifications from "vue-notification";
import VueGtag from "vue-gtag";
import VueTimeago from "vue-timeago";
import packageJson from "./../package.json";
import VueHighlightJS from "vue-highlight.js";
import xml from "highlight.js/lib/languages/xml";
import "highlight.js/styles/default.css";
import "highlight.js/";

Vue.use(VueHighlightJS, {
  languages: {
    xml
  }
});
Vue.use(Vuex);
Vue.use(VueGtag, {
  config: {
    id: "UA-88593536-3"
  }
});
Vue.use(VueResource);
Vue.use(Notifications);
Vue.use(VueTimeago, {
  locale: "cs",
  locales: {
    cs: require("date-fns/locale/cs")
  }
});

console.log(`Frontend version: ${packageJson.version}`);

export default Vue;
