import login from "./login";
import logout from "./logout";
import refreshUser from "./refresh-user";

export default {
  login,
  refreshUser,
  logout
};
