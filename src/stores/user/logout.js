import MUTATIONS from "./mutations";
import UserService from "./../../services/user";

export default async ({ commit }) => {
  try {
    await UserService.logout();
    commit(MUTATIONS.CLEAR_USER);
  } catch (e) {
    console.error("Unable to logout user");
    throw e;
  }
};
