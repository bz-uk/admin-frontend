import MUTATIONS from "./mutations";
import UserService from "./../../services/user";

export default async ({ commit }) => {
  try {
    let response = await UserService.get();
    commit(MUTATIONS.SET_USER, response.user);
  } catch (e) {
    console.error("User not set");
    throw e;
  }
};
