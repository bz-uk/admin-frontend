import MUTATIONS from "./mutations";
import UserService from "./../../services/user";

export default async ({ commit }, username, password, rememberMe) => {
  try {
    let response = await UserService.login(username, password, rememberMe);
    commit(MUTATIONS.SET_USER, response.data.user);
  } catch (e) {
    console.error("Unable to login user");
    throw e;
  }
};
