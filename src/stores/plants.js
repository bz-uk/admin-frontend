import Plants from "./../services/plants";

const MUTATIONS = {
  SET_PLANT: "setPlant",
  CLEAR_PLANT: "clearPlant"
};

export default {
  namespaced: true,
  state: {
    plants: {}
  },
  // getters: {
  //     familiesList: state => state.familiesList
  // },
  mutations: {
    [MUTATIONS.SET_PLANT]: (state, plant) => {
      state.plants[plant.id] = plant;
    },
    [MUTATIONS.CLEAR_PLANT]: (state, plantId) => {
      delete state.plants[plantId];
    }
  },
  actions: {
    async loadPlant({ commit, state }, plantId) {
      if (state.plants[plantId]) {
        return state.plants[plantId];
      }

      let response = await Plants.getOne(plantId);

      commit(MUTATIONS.SET_PLANT, response);
      return response;
    },
    clearPlant({ commit }, plantId) {
      commit(MUTATIONS.CLEAR_PLANT, plantId);
    }
  }
};
