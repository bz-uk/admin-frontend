import Tags from "../services/plant-tags";
import ArticleTagsService from "../services/article-tags";

const MUTATIONS = {
  SET_TAGS: "setTags",
  SET_ARTICLE_TAGS: "setArticleTags",
  SET_ARTICLE_TAGS_PROMISE: "setArticleTagsPromise",
  CLEAR_TAGS: "clearTags"
};

export default {
  namespaced: true,
  state: {
    //public
    tagsList: null,
    articleTagsList: null,
    articleTagsListById: null,
    //internal
    articleTagsListPromise: null
  },
  getters: {
    tagsList: state => state.tagsList,
    articleTagsList: state => state.articleTagsList,
    articleTagsListById: state => state.articleTagsListById
  },
  mutations: {
    [MUTATIONS.SET_TAGS]: (state, tagsList) => {
      state.tagsList = tagsList;
    },
    [MUTATIONS.CLEAR_TAGS]: state => {
      state.tagsList = null;
    },
    [MUTATIONS.SET_ARTICLE_TAGS]: (state, tagsList) => {
      state.articleTagsList = tagsList;
      let byId = {};
      for (let tag of tagsList) {
        byId[tag.id] = tag;
      }
      state.articleTagsListById = byId;
    },
    [MUTATIONS.SET_ARTICLE_TAGS_PROMISE]: (state, promise) => {
      state.articleTagsListPromise = promise;
    }
  },
  actions: {
    async loadTags({ commit, state }) {
      if (state.tagsList) {
        return state.tagsList;
      }

      let response = await Tags.get();

      commit(MUTATIONS.SET_TAGS, response.rows);
      return response;
    },
    async loadArticleTags({ commit, state }) {
      if (state.articleTagsList) {
        return state.articleTagsList;
      }
      if (state.articleTagsListPromise) {
        return state.articleTagsListPromise;
      }
      let request = ArticleTagsService.getAll();
      commit(MUTATIONS.SET_ARTICLE_TAGS_PROMISE, request);

      return request.then(response => {
        commit(MUTATIONS.SET_ARTICLE_TAGS, response.rows);
        return response.rows;
      });
    },
    clearTags({ commit }) {
      commit(MUTATIONS.CLEAR_TAGS);
    }
  }
};
