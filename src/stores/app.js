import { getBooleanWithDefault } from "./../util/local-storage";
import Settings from "../services/settings";
import packageJson from "./../../package.json";

const MUTATIONS = {
  SET_DRAWER_OPEN: "setDrawerOpen",
  SET_DRAWER_MINI: "setDrawerMini",
  SET_SETTINGS: "setSettings",
  SET_WEBP: "setWebp",
  SET_VERSION: "setVersion"
};

export default {
  namespaced: true,
  state: {
    drawerOpen: getBooleanWithDefault("drawerOpen", false),
    drawerMini: getBooleanWithDefault("drawerMini", false),
    settings: null,
    webp: false
  },
  getters: {
    drawerOpen: state => state.drawerOpen,
    drawerMini: state => state.drawerMini,
    settings: state => state.settings,
    webp: state => state.webp,
    version: state => state.version
  },
  mutations: {
    [MUTATIONS.SET_DRAWER_OPEN]: (state, open) => {
      state.drawerOpen = open;
      localStorage.setItem("drawerOpen", JSON.stringify(open));
    },
    [MUTATIONS.SET_DRAWER_MINI]: (state, mini) => {
      state.drawerMini = mini;
      localStorage.setItem("drawerMini", JSON.stringify(mini));
    },
    [MUTATIONS.SET_SETTINGS]: (state, settings) => {
      state.settings = settings;
    },
    [MUTATIONS.SET_WEBP]: (state, supported) => {
      state.webp = supported;
    },
    [MUTATIONS.SET_VERSION]: (state, version) => {
      state.version = version;
    }
  },
  actions: {
    setDrawerOpen({ commit }, open) {
      if (typeof open !== "boolean") {
        throw new Error("Value must be boolean!");
      }
      commit(MUTATIONS.SET_DRAWER_OPEN, open);
    },
    setDrawerMini({ commit }, mini) {
      if (typeof mini !== "boolean") {
        throw new Error("Value must be boolean!");
      }
      commit(MUTATIONS.SET_DRAWER_MINI, mini);
    },
    setWebpSupport({ commit }, supported) {
      commit(MUTATIONS.SET_WEBP, supported);
    },
    async loadSettings({ state, commit }) {
      if (state.settings) {
        return state.settings;
      }

      let response = await Settings.get();

      commit(MUTATIONS.SET_SETTINGS, response);
      commit(MUTATIONS.SET_VERSION, {
        frontend: packageJson.version,
        backend: response.appVersion
      });

      return response;
    }
  }
};
