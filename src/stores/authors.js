import Authors from "./../services/authors";

const MUTATIONS = {
  SET_AUTHORS: "setAuthors",
  CLEAR_AUTHORS: "clearAuthors"
};

export default {
  namespaced: true,
  state: {
    authorsList: null
  },
  getters: {
    authorsList: state => state.authorsList
  },
  mutations: {
    [MUTATIONS.SET_AUTHORS]: (state, authorsList) => {
      state.authorsList = authorsList;
    },
    [MUTATIONS.CLEAR_AUTHORS]: state => {
      state.authorsList = null;
    }
  },
  actions: {
    async loadAuthors({ commit, state }) {
      if (state.authorsList) {
        return state.authorsList;
      }

      let response = await Authors.getAll();

      commit(MUTATIONS.SET_AUTHORS, response);
      return response;
    },
    clearAuthors({ commit }) {
      commit(MUTATIONS.CLEAR_AUTHORS);
    }
  }
};
