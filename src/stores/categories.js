import Categories from "./../services/categories";

const MUTATIONS = {
  SET_CATEGORIES: "setCategories",
  CLEAR_CATEGORIES: "clearCategories"
};

export default {
  namespaced: true,
  state: {
    categoriesList: null
  },
  getters: {
    categoriesList: state => state.categoriesList,
    categoriesById: state => {
      let byIds = {};
      for (let cat of state.categoriesList) {
        byIds[cat.id] = cat;
      }
      return byIds;
    }
  },
  mutations: {
    [MUTATIONS.SET_CATEGORIES]: (state, categoriesList) => {
      state.categoriesList = categoriesList;
    },
    [MUTATIONS.CLEAR_CATEGORIES]: state => {
      state.categoriesList = null;
    }
  },
  actions: {
    async loadCategories({ commit, state }) {
      if (state.categoriesList) {
        return state.categoriesList;
      }

      let response = await Categories.get();

      commit(MUTATIONS.SET_CATEGORIES, response.rows);
      return response;
    },
    clearCategories({ commit }) {
      commit(MUTATIONS.CLEAR_CATEGORIES);
    }
  }
};
