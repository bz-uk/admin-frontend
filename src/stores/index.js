import user from "./user-store";
import app from "./app";
import categories from "./categories";
import tags from "./tags";
import families from "./families";
import authors from "./authors";
import plants from "./plants";
import languages from "./languages";
import apiKeys from './api-keys';

export default {
  user,
  languages,
  app,
  categories,
  tags,
  apiKeys,
  families,
  authors,
  plants
};
