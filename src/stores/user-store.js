/*
 * Copyright (c) 2019 Eva Novotná, Juraj Šimon
 */

import actions from "./user/index";
import MUTATIONS from "./user/mutations";

export default {
  namespaced: true,
  state: {
    user: null
  },
  getters: {
    user: state => state.user
  },
  mutations: {
    [MUTATIONS.SET_USER]: (state, user) => {
      state.user = user;
    },
    [MUTATIONS.CLEAR_USER]: state => {
      state.user = null;
    }
  },
  actions
};
