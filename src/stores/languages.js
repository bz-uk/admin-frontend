import { LanguageService } from "./../services/languages";

const MUTATIONS = {
  SET_LANGUAGES: "setLanguages"
};

export default {
  namespaced: true,
  state: {
    languages: null
  },
  getters: {
    languages: state => state.languages
  },
  mutations: {
    [MUTATIONS.SET_LANGUAGES]: (state, alnguages) => {
      state.languages = alnguages;
    }
  },
  actions: {
    async load({ commit, state }) {
      if (state.languages) {
        return state.languages;
      }

      let response = await LanguageService.getAll();

      let assocLanguages = {};
      for (let lang of response.rows) {
        assocLanguages[lang.two_letter_code] = lang;
      }

      commit(MUTATIONS.SET_LANGUAGES, assocLanguages);
      return assocLanguages;
    }
  }
};
