import { ApiKeysService } from "./../services/api-keys";

const MUTATIONS = {
  SET_API_KEYS: "setApiKeys",
  CLEAR_API_KEYS: "clearApiKeys"
};

export default {
  namespaced: true,
  state: {
    apiKeysList: null
  },
  getters: {
    apiKeysList: state => state.apiKeysList
  },
  mutations: {
    [MUTATIONS.SET_API_KEYS]: (state, list) => {
      state.apiKeysList = list;
    },
    [MUTATIONS.CLEAR_API_KEYS]: state => {
      state.apiKeysList = null;
    }
  },
  actions: {
    async load({ commit, state }) {
      if (state.apiKeysList) {
        return state.apiKeysList;
      }

      let response = await ApiKeysService.getAll();

      commit(MUTATIONS.SET_API_KEYS, response.rows);
      return response;
    },
    clearCategories({ commit }) {
      commit(MUTATIONS.CLEAR_API_KEYS);
    }
  }
};
