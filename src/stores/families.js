import Families from "./../services/families";

const MUTATIONS = {
  SET_FAMILIES: "setFamilies",
  CLEAR_FAMILIES: "clearFamilies"
};

export default {
  namespaced: true,
  state: {
    familiesList: null
  },
  getters: {
    familiesList: state => state.familiesList
  },
  mutations: {
    [MUTATIONS.SET_FAMILIES]: (state, familiesList) => {
      state.familiesList = familiesList;
    },
    [MUTATIONS.CLEAR_FAMILIES]: state => {
      state.familiesList = null;
    }
  },
  actions: {
    async loadFamilies({ commit, state }) {
      if (state.familiesList) {
        return state.familiesList;
      }

      let response = await Families.get();

      let rows = response.rows.map(row => {
        row.listName = `${row.name_lat} (${row.name})`;
        return row;
      });

      commit(MUTATIONS.SET_FAMILIES, rows);
      return response;
    },
    clearFamilies({ commit }) {
      commit(MUTATIONS.CLEAR_FAMILIES);
    }
  }
};
