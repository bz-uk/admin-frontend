import Vue from "./init";
import App from "./components/login";
import Error from "./components/error";
import vuetify from "./plugins/vuetify";
import store from "./store";
import { detectAndUpdateBody as detectWebp } from "./util/webp";
import VueI18n from "vue-i18n";
import {i18nMessages as messages} from "./i18n";
import {createAndMountVue} from "./util/vue";
// import {create as createRouterInstance} from "./routes";

Vue.config.productionTip = false;

Vue.use(VueI18n);

async function start() {
  let webpSupported = await detectWebp();
  try {
    await store.dispatch("app/loadSettings");
    store.dispatch("app/setWebpSupport", webpSupported);
    createAndMountVue(App);
  } catch (e) {
    console.error('Unable eto load settings', e);
    createAndMountVue(Error);
  }

}

start();