import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";

const LanguageService = {
  getAll() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/languages")
    ).getData();
  }
};
export default LanguageService;
export { LanguageService };
