import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init.js";
import axios from "axios";
const CancelToken = axios.CancelToken;

function _prepareRequestAndData(file, opts = {}) {
  let defaults = {
    uploadUrl: `upload/image`
  };
  let resolvedOpts = Object.assign({}, opts, defaults);
  const request = new ServerRequest(
    urlJoin(process.env.VUE_APP_BACKEND_URL, resolvedOpts.uploadUrl)
  );
  let formData = new FormData();
  formData.append("file", file);
  return { formData, request };
}

async function uploadSingleImage(file, opts = {}) {
  const { request, formData } = _prepareRequestAndData(file, opts);
  try {
    let response = await request.post(formData, undefined, {
      onUploadProgress: opts.onUploadProgress
    });
    return response.data;
  } catch (e) {
    Vue.notify({
      title: "Chyba",
      text: "Nepodařilo se nahrát obrázek",
      group: "app",
      type: "error"
    });
    throw e;
  }
}

function cancelableUploadSingleFile(file, opts) {
  const source = CancelToken.source();
  const { request, formData } = _prepareRequestAndData(file, opts);
  let response = request
    .post(formData, undefined, {
      cancelToken: source.token,
      onUploadProgress: opts.onUploadProgress
    })
    .then(response => {
      return response.data;
    });
  return {
    response,
    cancel: (msg = "Upload was cancelled") => {
      source.cancel(msg);
    }
  };
}

export { uploadSingleImage, cancelableUploadSingleFile };
