import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";

const TagsService = {
  get() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-tags-all")
    ).getData();
  },
  async nameExists(name) {
    let request = await new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-tags/search", {
        query: { name }
      })
    );
    let response = await request.getData();
    if (response && response.count > 0) {
      return true;
    }
    return false;
  },
  async update(tag) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-tags/${tag.id}`)
      );
      let { data } = await request.put({
        ...tag
      });
      Vue.notify({
        title: "Notifikace",
        text: "Tag upraven",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit tag",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async create(model) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-tags`)
      );
      let { data } = await request.post({
        ...model
      });
      Vue.notify({
        title: "Notifikace",
        text: "Tag vytvořen",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se vytvořit tag",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-tags-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async delete(tagId) {
    try {
      let result = await new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-tags/${tagId}`)
      ).delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Tag smazán",
        group: "app",
        type: "success"
      });
      return result;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat tag",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};

export default TagsService;

export {TagsService};
