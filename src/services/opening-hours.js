import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";

const OpeningHoursService = {
  async get() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/opening-hours")
    ).getData();
  },
  async update(from, to, open) {
    try {
      let request = new ServerRequest(
          urlJoin(process.env.VUE_APP_BACKEND_URL, "/opening-hours")
      )
      let { data } = await request.put({from, to, open});
      Vue.notify({
        title: "Notifikace",
        text: "Hodiny uloženy",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit hodiny",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
};

export default OpeningHoursService;
export { OpeningHoursService };
