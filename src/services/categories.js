import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";

const CategoriesService = {
  get() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-categories")
    ).getData();
  },
  async create(model) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-categories`)
      );
      let { data } = await request.post({
        ...model
      });
      Vue.notify({
        title: "Notifikace",
        text: "Kategorie vytvořena",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se vytvořit kategorii",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-categories-table-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async update(tag) {
    try {
      let request = new ServerRequest(
          urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-categories/${tag.id}`)
      );
      let { data } = await request.put({
        ...tag
      });
      Vue.notify({
        title: "Notifikace",
        text: "Kategorie upravena",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit kategorii",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async delete(tagId) {
    try {
      let result = await new ServerRequest(
          urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-categories/${tagId}`)
      ).delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Kategorie smazána",
        group: "app",
        type: "success"
      });
      return result;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat kategorii",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};

export default CategoriesService;
export { CategoriesService };
