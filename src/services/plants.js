import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init.js";

const PlantService = {
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plants-table-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async getMapMarkers(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plants-map-markers", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async getOne(plantId) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `plants/${plantId}`)
    ).getData();
  },
  async update(plant) {
    let request = new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `plants/${plant.id}`)
    );
    let { data } = await request.put(plant);
    Vue.notify({
      title: "Notifikace",
      text: "Rostlina uložena",
      group: "app",
      type: "success"
    });
    return data;
  },
  async delete(plantId) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `plants/${plantId}`)
      );
      let { data } = await request.delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Rostlina smazána",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat rostlinu",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};
export default PlantService;
export { PlantService };
