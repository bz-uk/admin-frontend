import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";
import { createSlug } from "../util/slug";

const GalleriesService = {
  // async create(model) {
  //   try {
  //     let request = new ServerRequest(
  //       urlJoin(process.env.VUE_APP_BACKEND_URL, `articles`)
  //     );
  //     let { data } = await request.post({
  //       ...model
  //     });
  //     Vue.notify({
  //       title: "Notifikace",
  //       text: "Článek vytvořen",
  //       group: "app",
  //       type: "success"
  //     });
  //     return data;
  //   } catch (e) {
  //     Vue.notify({
  //       title: "Chyba",
  //       text: "Nepodařilo se vytvořit článek",
  //       group: "app",
  //       type: "error"
  //     });
  //     throw e;
  //   }
  // },
  getGalleryPlantsWithImages(filter) {
    return new ServerRequest(
      urlJoin(
        process.env.VUE_APP_BACKEND_URL,
        `/gallery-plants-with-images-by-filter`
      )
    ).getData({ filter });
  },
  async get(id) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `/galleries/${id}`)
    ).getData();
  },
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/galleries-table-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async update(item) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `galleries/${item.id}`)
      );
      let { data } = await request.put({
        ...item
      });
      Vue.notify({
        title: "Notifikace",
        text: "Galerie uložena",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit galerií",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async delete(articleId) {
    try {
      let result = await new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `galleries/${articleId}`)
      ).delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Galerie smazána",
        group: "app",
        type: "success"
      });
      return result;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat galerii",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};

export default GalleriesService;
export { GalleriesService };
