import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";
import { createSlug } from "../util/slug";

const ArticlesService = {
  async titleValid(title) {
    let request = await new ServerRequest(
      urlJoin(
        process.env.VUE_APP_BACKEND_URL,
        `/validate/article/slug/${createSlug(title)}`
      )
    );
    let response = await request.getData();
    return response.valid;
  },
  async create(model) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `articles`)
      );
      let { data } = await request.post({
        ...model
      });
      Vue.notify({
        title: "Notifikace",
        text: "Článek vytvořen",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se vytvořit článek",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async get(id) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, `/articles/${id}`)
    ).getData();
  },
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/articles-table-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async update(tag) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `articles/${tag.id}`)
      );
      let { data } = await request.put({
        ...tag
      });
      Vue.notify({
        title: "Notifikace",
        text: "Článek upraven",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit článek",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async delete(articleId) {
    try {
      let result = await new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `articles/${articleId}`)
      ).delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Článek smazán",
        group: "app",
        type: "success"
      });
      return result;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat článek",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};

export default ArticlesService;
export { ArticlesService };
