import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";

export default {
  async get() {
    let request = new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/settings")
    )
    let {data, headers} = await request.get();
    if (headers['x-app-version']) {
      console.log(`Backend version ${headers['x-app-version']}`);
      data.appVersion = headers['x-app-version'];
    }
    return data;
  }
};
