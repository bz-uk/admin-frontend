import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";

export default {
  get() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/user")
    ).getData();
  },
  login(username, password, remember) {
    return new ServerRequest(urlJoin(process.env.VUE_APP_BACKEND_URL, "/login")).post({
      username,
      password,
      remember
    });
  },
  logout() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/logout")
    ).post();
  }
};
