import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";

const FamiliesSerivce = {
  get() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-families")
    ).getData();
  },
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/plant-families-table-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async create(model) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-families`)
      );
      let { data } = await request.post({
        ...model
      });
      Vue.notify({
        title: "Notifikace",
        text: "Čeleď vytvořena",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se vytvořit čeleď",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async update(tag) {
    try {
      let request = new ServerRequest(
          urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-families/${tag.id}`)
      );
      let { data } = await request.put({
        ...tag
      });
      Vue.notify({
        title: "Notifikace",
        text: "Čeleď upravena",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit čeleď",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async delete(tagId) {
    try {
      let result = await new ServerRequest(
          urlJoin(process.env.VUE_APP_BACKEND_URL, `plant-families/${tagId}`)
      ).delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Čeleď smazána",
        group: "app",
        type: "success"
      });
      return result;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat čeleď",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};

export default FamiliesSerivce;

export { FamiliesSerivce };
