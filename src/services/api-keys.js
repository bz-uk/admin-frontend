import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";

const ApiKeysService = {
  getAll() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/api-keys/all")
    ).getData();
  }
};

export default ApiKeysService;

export { ApiKeysService };
