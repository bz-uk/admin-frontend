import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";
import Vue from "../init";

const AuthorsService = {
  async create(model) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `authors`)
      );
      let { data } = await request.post({
        ...model
      });
      Vue.notify({
        title: "Notifikace",
        text: "Autor vytvořen",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se vytvořit autora",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async getAll() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/authors")
    ).getData();
  },
  getTableData(filter, page, rowsPerPage) {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/authors-table-data", {
        query: { page, rowsPerPage, ...filter }
      })
    ).getData();
  },
  async update(tag) {
    try {
      let request = new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `authors/${tag.id}`)
      );
      let { data } = await request.put({
        ...tag
      });
      Vue.notify({
        title: "Notifikace",
        text: "Autor upraven",
        group: "app",
        type: "success"
      });
      return data;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se uložit autora",
        group: "app",
        type: "error"
      });
      throw e;
    }
  },
  async delete(tagId) {
    try {
      let result = await new ServerRequest(
        urlJoin(process.env.VUE_APP_BACKEND_URL, `authors/${tagId}`)
      ).delete();
      if (result.status < 200 || result.status >= 400) {
        throw new Error(`Request failed with status code ${result.status}`);
      }
      Vue.notify({
        title: "Notifikace",
        text: "Autor smazán",
        group: "app",
        type: "success"
      });
      return result;
    } catch (e) {
      Vue.notify({
        title: "Chyba",
        text: "Nepodařilo se smazat autora$",
        group: "app",
        type: "error"
      });
      throw e;
    }
  }
};

export default AuthorsService;
export { AuthorsService };
