import { ROUTES } from "@/routes";
//import Vue from '@/init';

const MenuService = {
  getLeftDrawerMenuImage() {

  },
  getLeftDrawerMenuItems() {
    return [
      {
        title: "Dashboard",
        to: { name: ROUTES.HOME },
        icon: "mdi-view-dashboard"
      },
      {
        title: "Registr",
        items: [
          {
            to: { name: ROUTES.PLANTS_LIST },
            title: "Rostliny",
            icon: "mdi-flower"
          },
          {
            to: { name: ROUTES.TAGS_LIST },
            title: "Tagy",
            icon: "mdi-tag-multiple"
          },
          {
            to: { name: ROUTES.CATEGORIES_LIST },
            title: "Kategorie",
            icon: "mdi-call-split"
          },
          {
            to: { name: ROUTES.FAMILIES_LIST },
            title: "Čeledi",
            icon: "mdi-call-split"
          },
          {
            to: { name: ROUTES.AUTHORS_LIST },
            title: "Autoři",
            icon: "mdi-account-multiple"
          },
          {
            to: { name: ROUTES.GALLERIES_LIST },
            title: "Galerie",
            icon: "mdi-image-multiple"
          }
        ]
      },
        //DISABLED FOR NOW
      // {
      //   title: "Veřejný web",
      //   items: [
      //     {
      //       to: { name: ROUTES.ARTICLES_LIST },
      //       title: "Články",
      //       icon: "mdi-file-document-outline"
      //     },
      //     {
      //       to: { name: ROUTES.OPENING_HOURS },
      //       title: "Otevírací hodiny",
      //       icon: "mdi-clock-outline"
      //     }
      //   ]
      // },
      {
        title: "Ostatní",
        items: [
          {
            to: { name: ROUTES.ABOUT },
            title: "O aplikaci",
            icon: "mdi-information-outline"
          }
        ]
      }
    ];
  }
};

export default MenuService;
export { MenuService };
