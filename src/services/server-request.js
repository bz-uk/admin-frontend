import axios from "axios";
import Debug from "debug";

const debug = Debug("server-request");

export default class ServerRequest {
  constructor(url) {
    this.url = url;
  }
  //TODO: implement cancelation...
  /**
   *
   * @param method
   * @param data
   * @param params
   * @return {Promise<AxiosResponse<any>>}
   */
  baseCall(method, data, params, config = {}) {
    debug("Requesting data from ", method, this.url, data, params);
    let defaultConfig = {
      method: method,
      url: this.url,
      data,
      withCredentials: true,
      params,
      xsrfCookieName: "XSRF-TOKEN",
      validateStatus: function(status) {
        return status >= 200 && status < 400;
      }
    };
    let resolvedConfig = Object.assign({}, defaultConfig, config);
    return axios(resolvedConfig)
      .then(function(response) {
        return response;
      })
      .catch(err => {
        console.error("Error during " + method + " request", err);
        throw err;
      });
  }

  get(params) {
    return this.baseCall("get", undefined, params);
  }

  getData(params) {
    return this.baseCall("get", undefined, params).then(({ data }) => data);
  }

  delete(params) {
    return this.baseCall("delete", undefined, params);
  }

  put(data, params) {
    return this.baseCall("put", data, params);
  }

  post(data, params, config) {
    return this.baseCall("post", data, params, config);
  }
}
