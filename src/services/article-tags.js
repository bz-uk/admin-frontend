import ServerRequest from "./server-request";
import urlJoin from "proper-url-join";

const ArticleTagsService = {
  getAll() {
    return new ServerRequest(
      urlJoin(process.env.VUE_APP_BACKEND_URL, "/article-tags/all")
    ).getData();
  }
};

export default ArticleTagsService;

export { ArticleTagsService };
